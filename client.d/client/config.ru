require 'rack'
require 'redis'

redis = Redis.new

app = Proc.new do |env|
  req = Rack::Request.new(env)
  data = redis.get req.params["adid_h"] || "none"
  ['200', {'Content-Type' => 'text/json'}, [data]]
end
 
run app
