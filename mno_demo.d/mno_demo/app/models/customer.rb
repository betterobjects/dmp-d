class Customer < ActiveRecord::Base
  validates_presence_of :firstname, :lastname, :zip
  validates_uniqueness_of :firstname, :scope => :lastname
  validates_uniqueness_of :msin
  
  scope :optins, -> { where optout: false }
  
  #
  #
  def imsi
    "#{mcc}#{mnc}#{msin}"
  end
  
  #
  #
  def hashed_imsi
    @imsi_h ||= Digest::MD5.hexdigest imsi
  end
    
  #
  #
  def age
    Date.today.year - birthdate.year - (birthdate.to_date.change(:year => Date.today.year) > Date.today ? 1 : 0)
  end
    
end
