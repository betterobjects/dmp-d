class GatewayController < ApplicationController

  
  #
  #
  def index
    
    api_uri = "http://mno.mars.zeotap.com:8080/api"
    hasher_uri = "http://hasher.mars.zeotap.com:8080/api/v1/hasher/adid"    
    
    adid_h = params[:adid_h]
    
    unless adid_h  
      render text: "no adid_h in request", status: 500 
      return
    end
        
    if rand(10) == 0 
      render text: "just a random error", status: 500 
      return
    end
    
    imsi = JSON.parse(RestClient.get( api_uri, params: {ip: ip}, :content_type => :json ))['imsi']
    imsi_h1 = Digest::MD5.hexdigest imsi
    
    render text: RestClient.post( hasher_uri, {imsi_h1: imsi_h1, adid_h: adid_h }, :content_type => :json ).code
  end
  
  #
  #
  def create
    index
  end
  
  
  private 
  
  #
  #
  def ip
    ip ||= request.env['HTTP_FORWARDED']
    ip ||= request.env['HTTP_X_FORWARDED_FOR']
    ip ||= request.env['HTTP_CLIENT_IP']
    ip ||= request.env['REMOTE_HOST']
    raise "no ip in request" unless ip
    ip
  end
end