class ApiController < ApplicationController
  
  #
  #
  def index
    ip = params[:ip]

    unless ip 
      puts "no ip in request"
      render text: "no ip in request", status: 500 
      return
    end
    
    unless IPAddress.valid? ip
      puts "no valid ip"
      render text: "no valid ip", status: 500 
      return      
    end
    
    if rand(10) == 0 
      puts "just a random error"
      render text: "just a random error", status: 500 
      return
    end
    
    customer = Customer.find rand(Customer.count)
    imsi = customer.imsi
    json = {imsi: imsi, note: "this is a random (but existing) imsi"}
    render json: json
  end
  
  #
  #
  def create
    index
  end

end