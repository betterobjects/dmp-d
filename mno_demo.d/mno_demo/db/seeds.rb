# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

count = 0 
1000.times do 
  print '.' if count % 100 == 0 
  customer = Customer.new firstname: Faker::Name.first_name, 
    lastname: Faker::Name.last_name, 
    birthdate: (Date.today - 80.years + rand(64).years + rand(12).months + rand(365).days), 
    gender: ['m', 'f'].sample,
    mcc: '262',
    mnc: '02',
    msin: "#{1 + rand(9)}#{rand(10000000000)}",
    zip: Faker::Address.zip.first(5),
    street: "#{Faker::Address.street_name} #{rand(999)}",
    optout: rand(30) == 0, 
    note: Faker::Lorem.sentence(5)
  
  count += 1 if customer.save
end
puts "#{count} customers created"