class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :firstname
      t.string :lastname
      t.date :birthdate
      t.string :gender
      t.string :mcc
      t.string :mnc
      t.string :msin
      t.string :zip
      t.string :street
      t.text :note
      t.boolean :optout

      t.timestamps
    end
  end
end
