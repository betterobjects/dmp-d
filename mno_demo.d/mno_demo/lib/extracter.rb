module Extracter 
  
  @@uri = "http://supplier.mars.zeotap.com:8080/api/v1/supplier/profile.json"
  
  #
  #
  def self.extract_and_send
      
    count = 0 
    Customer.optins.each do |customer|
      count += 1 if send_customer customer
    end
    puts "#{count} customers sent" 
  end

  private 
  
  #
  #
  def self.send_customer customer
    begin
      
      data = {imsi_h1: customer.hashed_imsi, gender: customer.gender, zip: customer.zip.first(3), age: customer.age }
      response = RestClient.post @@uri, data.to_json, :content_type => :json
            
      return true if response.code == 200
        
      puts "Error in response for customer: #{customer.id} - #{response.code}"
    rescue Exception => e
      puts "Error sending customer: #{customer.id}"
    end    
  end
end
