#!/bin/bash

cd $1
pwd
echo -n -e "\033]0;$2 ($3)\007"
rm -rf tmp/pids/*
rails s -p $3 -P tmp/pids/server_$3.pid