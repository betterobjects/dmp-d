# Rails.application.reload_routes!
# routes = Rails.application.routes.url_helpers
# uncomment the above to get routes here
DMP = YAML.load_file( File.join( Rails.root, 'config', 'dmp.yml') )[Rails.env]

server_config_file = File.join( '/', 'etc', 'mars.yml' )
if File.exist? server_config_file
  CONFIG = YAML.load_file server_config_file
  DMP['roles'] = DMP['roles'] & CONFIG['roles']
else
  raise "#{server_config_file} is missing" unless Rails.env.development?
end