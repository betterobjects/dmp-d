Rails.application.routes.draw do
  
  namespace :api do
    namespace :v1 do

      if DMP['roles'].include? 'supplier'
        post  'supplier/profile', to: '/api/supplier#receive_profile'
        get   'supplier/profile', to: '/api/supplier#send_profile'
      end
  
      if DMP['roles'].include? 'matcher'
        post  'matcher/adid', to: '/api/matcher#adid'
      end
      
      if DMP['roles'].include? 'hasher'
        post  'hasher/adid', to: '/api/hasher#adid'
      end
  
      if DMP['roles'].include? 'master'
        post  'master/profile', to: '/api/master#profile'
      end
      
    end
  end
end
