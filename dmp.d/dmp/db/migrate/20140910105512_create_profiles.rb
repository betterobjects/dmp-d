class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :imsi_h2e
      t.string :gender
      t.string :zip
      t.string :age

      t.timestamps
    end
    
    add_index :profiles, :imsi_h2e
  end
end
