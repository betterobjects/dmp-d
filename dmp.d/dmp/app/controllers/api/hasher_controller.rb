class Api::HasherController < ApiController
  
  #
  #
  def adid
    
    matcher_uri = DMP['hosts']['matcher'] + '/api/v1/matcher/adid'
    
    imsi_h1 = params[:imsi_h1]
    imsi_h2 = Digest::SHA3.hexdigest imsi_h1 + DMP['secrets']['salt']
    
    data = {adid_h: params[:adid_h], imsi_h2: imsi_h2 }
        
    render text: RestClient.post( matcher_uri, data, :content_type => :json ).code
  end
  
end