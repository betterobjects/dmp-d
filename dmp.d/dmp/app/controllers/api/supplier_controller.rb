class Api::SupplierController < ApiController

  #
  #
  def receive_profile
          
    imsi_h2 = Digest::SHA3.hexdigest params[:imsi_h1] + DMP['secrets']['salt']
    imsi_h2e = URI::encode(Base64.strict_encode64(imsi_h2.encrypt :symmetric, password: DMP['secrets']['password']))
    
    profile = Profile.where( imsi_h2e: imsi_h2e ).first
    if profile
      profile.destroy 
      Rails.logger.debug "profile #{profile.imsi_h2e} found and destroyed"
    end
    
    profile = Profile.new imsi_h2e: imsi_h2e, gender: params[:gender], zip: params[:zip], age: params[:age]
    
    if profile.save
      Rails.logger.debug "profile #{profile.imsi_h2e} created"
      render text: 'ok'
    else
      Rails.logger.debug "profile #{profile.imsi_h2e} could not be saved"
      render text: "could not save profile", status: 500
    end
    
  end
  
  #
  #
  def send_profile
    imsi_h2e = params[:imsi_h2e] 
    
    profile = Profile.where( imsi_h2e: imsi_h2e ).first
    if profile
      render json: profile
    else
      render text: "no profile for #{imsi_h2e} found", status: 404
    end
    
  end
end