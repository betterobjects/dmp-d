class Api::MatcherController < ApiController

  #
  #
  def adid
    
    supplier_uri = DMP['hosts']['supplier'] + '/api/v1/supplier/profile'
    master_uri = DMP['hosts']['master'] + '/api/v1/master/profile'
    
    adid_h = params[:adid_h]
    imsi_h2 = params[:imsi_h2]  
    
    imsi_h2e = URI::encode(Base64.strict_encode64(imsi_h2.encrypt :symmetric, password: DMP['secrets']['password']))
        
    data = RestClient.get( supplier_uri, params: {imsi_h2e: imsi_h2e}, :content_type => :json )

    data = JSON.parse data
    data[:adid_h] = adid_h
    data[:imsi_h2e] = nil
        
    render text: RestClient.post( master_uri, data, :content_type => :json ).code
  end
end