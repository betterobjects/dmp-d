#!/bin/bash

DIR=$(pwd)

function new_tab(){
  osascript -e "
    tell application \"System Events\" to tell process \"Terminal\" to keystroke \"t\" using command down
    tell application \"Terminal\" to do script \"cd $DIR; $1\" in selected tab of the front window
    " 
}

new_tab "./start_server.sh mno_demo.d/mno_demo demo 3000"
new_tab "./start_server.sh mno_demo.d/mno_demo demo 3001"
new_tab "./start_server.sh dmp.d/dmp supplier 3002"
new_tab "./start_server.sh dmp.d/dmp matcher 3003"
new_tab "./start_server.sh dmp.d/dmp hasher 3004"
new_tab "./start_server.sh dmp.d/dmp master 3005"
new_tab "./start_console.sh mno_demo.d/mno_demo demo_console"

