class AddAffiliatenIdAndOptouttimeToOptins < ActiveRecord::Migration
  def change
    add_column :optins, :affiliate_id, :string
    add_column :optins, :optout_time, :timestamp
  end
end
