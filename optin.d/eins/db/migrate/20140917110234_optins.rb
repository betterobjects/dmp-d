class Optins < ActiveRecord::Migration
  
  def change
    create_table :optins do |t|
      t.integer  "tracked_advertiser_id"
      t.string   "mobile"
      t.text     "token"
      t.string   "transaction_id"
      t.boolean  "has_optin",             default: false
      t.boolean  "sms_sent",              default: false
      t.boolean  "optout",                default: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "country_code"
      t.text     "user_comment"
      t.string   "hostname"
    end    
  end
  
end
