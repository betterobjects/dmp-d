class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.string :code
      t.integer :optin_id

      t.timestamps
    end
    add_index :vouchers, :code, unique: true
  end
end
