Rails.application.routes.draw do
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  resources :optouts
  resources :pages
  resources :optins do
    member do # beware token will be id!
      get 'pending'
      get 'confirm'
      get 'confirmed'      
    end
  end  
  
  get 'mno_lookup/:mobile' => 'mno_lookup#lookup'
  
  root 'optins#new'
    
end
