namespace :import do

  desc 'This task imports voucher code csv'
  task :import_voucher_csv, [:csv_file] => :environment do |t, args|
    Zeopt::Voucher.load_vouchers args[:csv_file]
  end

end