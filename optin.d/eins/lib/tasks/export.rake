namespace :export do

  desc 'This task exports all valid optins for all advertiser'
  task :export_all, [:export_dir] => :environment do |t, args|
    Exporter.export_all args[:export_dir]
    puts "Exported all optin data for all advertisers"
  end

  desc 'This task exports all valid optins for given advertiser'
  task :export, [:t_id, :export_dir] => [:environment] do |t, args|
    begin
      unless args[:t_id]  
        puts "usage: rake export:export[<Advertiser ID>, [<Export Directory>]]"
        Exporter.list_advertisers
      else
        tracked_id = args[:t_id].to_i
        raise "must be number" if tracked_id == 0
        
        lines = Exporter.export(tracked_id, args[:export_dir])
        puts "Exporter wrote #{lines} lines"      
      end
    rescue Exception => e
      puts e
    end  
  end

  desc 'Lists all registered advertisers with id'
  task :list_advertisers => :environment do 
    Exporter.list_advertisers
  end

end
