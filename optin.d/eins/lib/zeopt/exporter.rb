module Zeopt
  
  module Exporter
  
    # 
    #  
    def self.export_all(export_dir="export")
      Advertiser.all.pluck(:id).each do |id|
        Exporter.export(id, export_dir)
      end
    end

    # 
    #
    def self.export(tracked_advertiser_id, export_dir="export")
      advertiser = Advertiser.where(id: tracked_advertiser_id).first
      unless advertiser 
        puts "Advertiser with #{tracked_advertiser_id} id is not known"
        Exporter.list_advertisers
        # return number of lines written
        return 0
      end

      optins = tracked_advertiser_id ? Optin.where(tracked_advertiser_id: tracked_advertiser_id) : Optin
      optins = optins.where(has_optin: true)
      optins = optins.where(optout: false)
      optins = optins.order :updated_at
    
      counter = 0
    
      unless optins.count > 0 
        puts "No optins found for '#{advertiser.name}'"
      else 
        puts "Exporting optins for '#{advertiser.name}'"
      
        unless export_dir.first == "/"
          file = File.join Rails.root, export_dir 
        else
          file = export_dir
        end

        FileUtils.mkdir_p file unless File.directory? file
        file = File.join file, "#{advertiser.name}_#{Time.now}".parameterize + ".csv"

        CSV.open(file, 'w') do |csv|
          csv << ["id","landingpage_id","country_code", "mobile", "timestamp"]
          optins.each do |optin|
            csv << [optin.id, optin.landingpage_id, optin.country_code, optin.mobile, optin.updated_at]
            counter+=1
          end      
        end
      end
      counter
    end
  
    # 
    #
    def self.list_advertisers
      Optin.uniq(:tracked_advertiser_id).order(:tracked_advertiser_id).pluck(:tracked_advertiser_id).each do |ad_id|
        puts "#{ad_id} - #{Advertiser.find(ad_id).name}"
      end
    end
  end
  
end