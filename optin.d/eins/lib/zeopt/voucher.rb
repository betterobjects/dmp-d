module Zeopt
  
  module Voucher

    #
    # load vouchers from CSV file
    #
    def self.load_vouchers filename

      saved = skipped = 0
      
      csv_text = File.read(filename)
      csv = CSV.parse(csv_text, :headers => true)
      csv.each do |row|
        voucher = Voucher.create(row.to_hash)
        voucher.save ? saved += 1 : skipped += 1
      end
      puts "#{saved} voucher codes loaded / #{skipped} voucher codes skipped"
    end

  end
end