module Zeopt
  class OptinEmbargoValidator < ActiveModel::Validator

    def validate(record)

      return unless record.new_record?
                   
      hashed = Optin.hashed_number record.country_code, record.mobile 
      hashed_mobiles = Optin.where(mobile: hashed).order("#{:created_at} DESC")

      return if hashed_mobiles.empty?

      # does mobile already exist?
      # check hashed mobile existence     
      record.errors.add :mobile, :too_many_optouts if hashed_mobiles.size >= options[:max_optouts]

      if (Time.now - hashed_mobiles.first.updated_at) < options[:minimal_time].to_i
        record.errors.add :mobile, :optin_time_to_short 
      end
    end
  
  end
end
