# encoding: utf-8

module Zeopt
  module Sms
    
    #
    #
    def self.send_sms optin
      begin
        Timeout::timeout(10) do
          mobile = "#{optin.country_code}#{optin.mobile}"          
          sms_text = I18n.t('sms.double_optin_text', url: tiny_url(optin.token))
          
          uri = "https://api.doingdata.net/sms/send"
          uri << "?api_service_key=#{ZEOPT['sms']['api_key']}"
          uri << "&msg_senderid=#{ZEOPT['sms']['sender_id']}"
          uri << "&msg_to=#{mobile}"
          uri << "&msg_text=#{sms_text}"
          uri << "&msg_clientref=abcdef123456&msg_dr=0&output=json"

          response = RestClient.get URI.encode(uri)
          data = JSON.parse(response)
          data['status'].eql?("OK")                  
        end
      rescue Exception => e
        Log.error "SMS", e
      end
    end
    
    private 
    
    #
    #
    def self.tiny_url token
      url = "http://tinyurl.com/api-create.php?url=#{Rails.application.routes.url_helpers.confirm_optin_path(token)}"
      RestClient.get url
    end
    
  end
end