module Zeopt
  module Mobile
    
    #
    #
    def self.remove_trailing_zeros(value)    
     s=value.gsub(/^0+/,'')
     s.gsub!("-",'')
     s.gsub!("/",'')
     s.gsub(/\s+/,'')
    end
    
    # 
    # lookup DNS of user IP and store in model
    #
    def self.resolv_hostname request       
      begin 
        ip_addr = request.env['REMOTE_ADDR']
        hostname = Resolv.getname(ip_addr)
        hostname.split('.').last(2).join('.')
      rescue Exception => e
        Rails.logger.error e
        "n/a"
      end
    end
  
    #
    # create uniform sample of n mobile numbers from Optin
    #
    def self.sample_mobile n
      sample = []
      Optin.all.sample(n).each do |optin| 
         sample << optin.country_code + optin.mobile 
      end
      sample
    end

    #
    # -1 error occured
    #  0 mccmc not valid 
    #  1 mccmc valid 
    #
    def self.validate_mno msisdn, mccmnc
      
      begin

        Timeout::timeout(10) do 

          return -1 unless rep = lookup_number(msisdn)
          return rep[:mccmnc] == mccmnc ? 1 : 0 

        end
      
      rescue Exception => e 
        Zeopt::Log.error "Telapi", e
        return -1
      
      end
      
    end


    #
    # query number portability lookup service for specific MSISDN
    #
    def self.lookup_number msisdn
      Telapi.config do |config|
        config.account_sid = 'AC4c8890845fc54f6565cd4d7fbb0eb49a'
        config.auth_token  = 'd91107d785474921a0c30c789ea43e83'
      end

      response = Telapi::Carrier.lookup( msisdn )
      response.carrier_lookups.first.symbolize_keys      
    end
    
    #
    #
    def self.hash_value value
       Digest::SHA3.hexdigest value
    end  
    
  end
end