module Zeopt
  module Log
    #
    #
    def self.error s, e
     Rails.logger.error "= #{s} ================================================================"
     Rails.logger.error e
     Rails.logger.error "===================================================================================="
    end
  end
end