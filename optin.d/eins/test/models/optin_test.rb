require "test_helper"

describe Optin do
    
  fixtures :optins
  let(:optin) { optins(:optin1) }
  
  it "should have a hashed mobile after optout" do
    assert optin.valid?
    
    hashed = Optin.hashed_number optin.country_code, optin.mobile
    
    optin.register_optout "just a test"
    assert optin.save
    
    assert_equal optin.mobile, hashed
  end

end