require "test_helper"

describe Voucher do
    
  fixtures :optins
  fixtures :vouchers
  
  it "should select invalid vouchers" do
        
    assert Voucher.invalid_vouchers.count == 1

  end

end