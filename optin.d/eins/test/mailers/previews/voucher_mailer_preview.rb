# Preview all emails at http://localhost:3000/rails/mailers/voucher_mailer
class VoucherMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/voucher_mailer/mail_current_optouts
  def mail_current_optouts
    VoucherMailer.mail_current_optouts
  end

end
