require "test_helper"

class VoucherMailerTest < ActionMailer::TestCase
  def test_mail_current_optouts
    mail = VoucherMailer.mail_current_optouts
    assert_equal "Invalid Voucher Codes", mail.subject
    assert_equal ["martin.held@hitfoxgroup.com"], mail.to    
    assert_match "Zeotap", mail.body.encoded
  end

end
