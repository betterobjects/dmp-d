require "test_helper"

# save_and_open_page    

#
# we dont send sms for real
#
module Zeopt::Sms
  def self.send_sms optin
    true
  end
end


feature "User opts in" do

  fixtures :vouchers
  fixtures :optins
    
  #
  #
  scenario "starts the page" do
    visit '/'
    page.must_have_content "Sichern Sie sich jetzt Ihre Belohnung"
    page.wont_have_content "Das ist nicht drin"
  end
  
  #
  #
  scenario "with invalid number" do
    visit '/optins/new'
    click_button "MEINE BELOHNUNG SICHERN!"
    page.must_have_content "Sichern Sie sich jetzt Ihre Belohnung"

    page.must_have_content "2 errors occurred"  
  end
  
  #
  #
  scenario "does not accept terms" do  
    visit '/optins/new'
    select '49', :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => '1736992000'
    click_button "MEINE BELOHNUNG SICHERN!"
    
    page.must_have_content "1 error occurred"
    page.must_have_content "Please accept our terms and conditions"    
  end
  
  #
  #
  scenario "with valid number", js: true do
    visit '/optins/new'
    
    page.wont_have_selector('p#wait_image')
    
    select '49', :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => '1736992000'
    check 'optin_terms'
    page.must_have_content "Die Datenverarbeitung erfolgt ausschließlich innerhalb der Europäischen Union"
    page.must_have_selector('a#confirm' )
    
    begin
      click_button "MEINE BELOHNUNG SICHERN!"
      fail "should not be able to click"
    rescue Capybara::Webkit::ClickFailed
    rescue Capybara::ElementNotFound # selenium
    end    
        
    page.find('a#confirm').click

    Zeopt::Mobile.stub(:validate_mno, 0 ) do
      click_button "MEINE BELOHNUNG SICHERN!"
      page.must_have_content "Your carrier is not supported"
    end 

    Zeopt::Mobile.stub(:validate_mno, 1 ) do
      RestClient.stub(:get, "{status: 'ok'}") do
        click_button "MEINE BELOHNUNG SICHERN!"    
        sleep(1)
        optin = Optin.last
        page.current_path.must_equal "/optins/#{optin.token}/pending"
        page.must_have_content "Sie erhalten nun eine SMS"
        refute optin.has_optin  
      end
    end 
    
    visit "/optins/#{Optin.last.token}/confirm"
    page.must_have_content "Geschafft!"
    
    optin = Optin.last
    assert optin.has_optin, "eigentlich double optin gesetzt"
        
  end
  
  #
  #
  scenario "with an optouted number", js: true do
    country_code = '49'
    mobile = '1736992008'
    optin = Optin.create! country_code: country_code, mobile: mobile, has_optin: true, sms_sent: true
    optin.register_optout "just a test"
    optin.save!
    Optin.connection.commit_db_transaction # anti pattern? 
    
    visit '/optins/new'
    select country_code, :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => mobile
    check 'optin_terms'
    page.find('a#confirm').click

    click_button "MEINE BELOHNUNG SICHERN!"
    page.must_have_content "Waiting time for new optin not reached yet"
    
  end


      
end

