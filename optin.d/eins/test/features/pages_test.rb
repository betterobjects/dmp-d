require "test_helper"

feature "User visits landing page" do
  
  scenario "visits home page" do
    visit '/'
    page.must_have_content "Sichern Sie sich jetzt Ihre Belohnung"
  end
  
  scenario "visits imprint" do
    visit '/'
    click_link "Impressum"
    page.must_have_content "Amtsgericht Charlottenburg; HRB 158127 B"
  end
  
  scenario " visit privacy" do
    visit '/'
    click_link "Datenschutzerklärung"
    page.must_have_content "Die BD Internet GmbH nimmt den Schutz personenbezogener Daten sehr ernst"
    page.must_have_content "Stand: Mai 2014"
  end

  scenario "visits faq" do
    visit '/'
    click_link "FAQ"
    page.must_have_content "Was passiert, nachdem ich meine Einwilligung gegeben habe"
  end
  
  scenario "visits terms" do
    visit '/'
    page.find('#nav a', text: "Teilnahmebedingungen" ).click 
    page.must_have_content "Ich willige ein, dass die BD Internet GmbH (BD Internet) die von mir in der Eingabemaske angegebenen Daten (Mobilfunknummer, Mobilfunkanbieter) erhebt, verarbeitet und nutzt"

    # without voucher campaign running
    Voucher.delete_all
    visit '/'
    page.wont_have_selector('#terms a')
    
    # with voucher campaign running
    ZEOPT['min_residual_vouchers'].times do |i| 
      Voucher.create! code: "code #{i}"
    end

    visit '/'
    
    page.find('#terms a', text: "Teilnahmebedingungen" ).click     
    page.must_have_content "Ich willige ein, dass die BD Internet GmbH (BD Internet) die von mir in der Eingabemaske angegebenen Daten (Mobilfunknummer, Mobilfunkanbieter) erhebt, verarbeitet und nutzt"
  end

  scenario "visits optout" do
    visit '/'
    click_link "Abmeldung"
    page.must_have_content "Um sich abzumelden, geben Sie bitte hier Ihre Mobilfunknummer ein"
  end

  
end
