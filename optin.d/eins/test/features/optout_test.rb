require "test_helper"

feature "User want to optout" do

  before(:each) do
    DatabaseCleaner.clean
  end
  
  #
  #
  scenario "calls optout page" do
    visit('/optouts/new')
    page.must_have_content("Um sich abzumelden, geben Sie bitte hier Ihre Mobilfunknummer ein")
  end
  
  #
  #
  scenario "enters no number" do
    visit('/optouts/new')
    click_button "ABMELDEN"
    page.must_have_content "Wir konnten diese Telefonnummer nicht finden"
  end
  
  #
  #
  scenario "enters unknown number" do    
    visit('/optouts/new')
    select '49', :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => '1736992005'
    click_button "ABMELDEN"
    page.must_have_content "Wir konnten diese Telefonnummer nicht finden"
  end

  #
  #
  scenario "enters a valild number" do
    optin = Optin.create! country_code: '49', mobile: '1736992007'

    visit('/optouts/new')
    select optin.country_code, :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => optin.mobile
    click_button "ABMELDEN"
    
    page.must_have_content "Abmeldung erfolgreich"    
  end

  #
  #
  scenario "enters a valild number twice" do
    optin = Optin.create! country_code: '49', mobile: '1736992009'

    visit('/optouts/new')
    select optin.country_code, :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => optin.mobile
    click_button "ABMELDEN"
    
    page.must_have_content "Abmeldung erfolgreich"
    
    visit('/optouts/new')
    select optin.country_code, :from => 'optin_country_code'
    fill_in 'Mobilfunknummer', :with => optin.mobile
    click_button "ABMELDEN"
    
    page.must_have_content "Wir konnten diese Telefonnummer nicht finden" # really? 
  end
  
  
end
  
