class OptinsController < ApplicationController

  before_action :optin_by_token, only: [:pending, :confirm, :confirmed]
  
  #
  #
  def index
    redirect_to new_optin_path
  end

  #
  #
  def new    
    # check if there are enough voucher codes left to be played out
    @optin = Optin.new if Voucher.available?    
  end
  
  #
  #
  def create    
    @optin = Optin.new optin_params

    if (not params[:mno_checked] == 'true') and @optin.valid?
      params[:lookup] = true 
      render "new"
      return
    end

    if @optin.save      
      if Zeopt::Sms.send_sms @optin
        @optin.update_attribute :sms_sent, true
        redirect_to pending_optin_path(@optin.token)
        return
      else
        # ja was dann? 
        @optin.errors.add( :base, I18n.t('sms.could_not_be_sent') )
      end
    end        
    render "new"
  end
  
  #
  #
  def pending
  end

  
  #
  #
  def confirm
    @optin.register_double_optin Zeopt::Mobile.resolv_hostname(request)

    if @optin.save
      redirect_to confirmed_optin_path(@optin.token)
    else
      raise "could not safe optin #{@optin.id} for confirmation"
    end
  end
  
  #
  #
  def confirmed
    unless @voucher = Voucher.find_by_optin_id(@optin.id)
      @voucher = Voucher.take_voucher @optin
    end
  end
    
  private 
  
  #
  #
  def optin_by_token
    @optin = Optin.find_by_token(params[:id])
    raise "no optin found" unless @optin
    @optin
  end
  
  #
  #
  def optin_params
    params.require(:optin).permit(:mobile, :tracked_advertiser_id, :terms, :transaction_id, :country_code, :user_comment)
  end
  
end


