#
#
class OptoutsController < ApplicationController
  
  #
  #
  def index
    redirect_to new_optout_path
  end
  
  #
  #
  def new  
    @optin = Optin.new
  end

  #
  #
  def create
    form = Optin.new optin_params
    optin = Optin.where(country_code: form.country_code, mobile: Zeopt::Mobile.remove_trailing_zeros(form.mobile)).first

    if optin
      optin.register_optout params[:user_comment]
      optin.save!
      redirect_to action: 'show', id: optin.token
    else
      @optin = form
      @optin.errors.add( :mobile, :mobile_not_found )
      render 'new'
    end
  end
  
  #
  #
  def show
    @optin = Optin.find_by_token(params[:id])
    raise "no optin found" unless @optin    
  end
  
  private
  
  #
  #
  def optin_params
    params.require(:optin).permit(:mobile, :country_code, :user_comment)
  end
  
end