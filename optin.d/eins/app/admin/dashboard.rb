ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #  span class: "blank_slate" do
    #    span I18n.t("active_admin.dashboard_welcome.welcome")
    #    small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #  end
    # end

    div do
        h2 "Optins per Day"
        @optins_per_day = Optin.where(optout: false).where(has_optin: true).group_by_day(:created_at).count
        render partial: 'admin/line_graph', locals: {metric: @optins_per_day}
    end
    
    div do
        h2 "Optouts per Day"
        @optouts_per_day = Optin.where(optout: true).group_by_day(:updated_at).count
        render partial: 'admin/line_graph', locals: {metric: @optouts_per_day}
    end


    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
