ActiveAdmin.register Optin do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  permit_params :has_optin, :optout, :country_code, :mobile if Rails.env.development?

  index do
    selectable_column
    id_column
    column :tracked_advertiser_id
    column :country_code
    column :mobile              do |optin| optin.mobile.truncate(14) end
    column :transaction_id      do |optin| optin.transaction_id.truncate(5) rescue "n/a" end
    column :has_optin
    column :sms_sent
    column :optout
    # column :user_comment
    column :created_at          do |optin| l optin.created_at, format: :short end
    actions
  end

  
end
