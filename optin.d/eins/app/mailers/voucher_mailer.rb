class VoucherMailer < ActionMailer::Base
  default from: ZEOPT['mail_from']

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.voucher_mailer.mail_current_optouts.subject
  #
  def mail_current_optouts
    
    csv="code;created_at\n"
    # TODO: Change the next line to return only relevant voucher codes
    # get all optins where creation_date and optout_date are smaller than 24h
    # and now - creation data <= 24h
    
    voucher_select = Voucher.invalid_vouchers
    voucher_select.each{|v| csv+= "#{v[:code]};#{v.optin[:optout_time]}\n"}

    attachments['voucher_code.csv'] = {content: csv}
    mail to: ZEOPT['mail_to'], subject: "Invalid Voucher Codes"
  end
end