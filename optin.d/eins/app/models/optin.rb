class Optin < ActiveRecord::Base
    
  has_one :voucher
    
  validates_format_of     :mobile, with: /\A[0-9 \-]+\z/, :message =>  :please_enter_correct_number, on: :create
  validates_length_of     :mobile, :minimum => 8, :maximum => 20, :message => :please_enter_correct_number,  on: :create
  validates_acceptance_of :terms, :message => :please_accept_terms_and_conditions
  validates_presence_of   :country_code, :message => :please_enter_country_code  
  validates_uniqueness_of :mobile, :scope => [:country_code], :message => :already_registered, on: :create

  validates_with Zeopt::OptinEmbargoValidator, 
    max_optouts: ZEOPT['optin_embargo_max_optouts'], 
    minimal_time: ZEOPT['minimal_optin_time']

  before_create :set_token
  
  #
  #
  def mobile=(value)
    self[:mobile] = Zeopt::Mobile.remove_trailing_zeros value
  end

  #
  #
  def register_double_optin hostname
    self.has_optin = true
    self.hostname = hostname
  end

  #
  #
  def register_optout user_comment
    self.optout = true
    self.optout_time = Time.now
    # don't use setter, it can change this by remove_trailing_zeros
    self[:mobile] = Optin.hashed_number self.country_code, self.mobile
    self.user_comment = user_comment
  end

  #
  #
  def self.hashed_number country_code, mobile
     Zeopt::Mobile.hash_value "#{country_code}#{mobile}"
  end

  private
  
  #
  #
  def set_token
    self.token = SecureRandom.urlsafe_base64(nil, false)
  end
    
end