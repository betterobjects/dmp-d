class Voucher < ActiveRecord::Base
  belongs_to :optin
  validates_uniqueness_of :code
  
  #
  #
  def self.available?
    Voucher.where(optin_id: nil).count >= ZEOPT['min_residual_vouchers']
  end
  
  #
  # returns next free voucher code and assigns optin id, returns nil if no voucher left
  #
  def self.take_voucher optin
  
    Voucher.transaction do    
     if voucher = Voucher.where(optin_id: nil).order(:created_at).first
       voucher.update_attribute :optin_id, optin.id
       voucher
     end
    end
  end
  

  #
  #
  def self.invalid_vouchers 

    # TODO: add optoutdate to model
    vouchers = Voucher.includes(:optin)
    vouchers = vouchers.where("optins.optout" => true)
    vouchers = vouchers.where(["optins.optout_time - optins.created_at <= interval '? hours'", 24])
    vouchers = vouchers.where(["optins.created_at >= ?",Time.now - 24.hours])
    vouchers
  end
  
end
