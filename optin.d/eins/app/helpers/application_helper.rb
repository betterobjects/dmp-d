module ApplicationHelper

  #
  #
  def transaction_id_params    
    if (params[:optin])
      t_id = params[:optin][:transaction_id] ? params[:optin][:transaction_id] : ""
    else
      t_id = params[:transaction_id] ? params[:transaction_id] : ""
    end
    t_id
  end

  #
  #
  def generate_country_code_array(codestr)
    a = codestr.gsub(" ","").split(",")
    a.map{|k| "+"+k }.zip(a)
  end
  
end
